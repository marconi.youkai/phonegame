﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spikes : MonoBehaviour
{
    private Transform spikeTransform;
    [SerializeField] private float jitterAmount = 0.0f;

    private void Start()
    {
        this.spikeTransform = this.transform;

        // StartCoroutine(spikeJitter());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            SceneManager.LoadScene("First level");
        }
    }

    private IEnumerator spikeJitter()
    {
        while (true)
        {
            spikeTransform.position = new Vector2(spikeTransform.position.x + Mathf.Sin(jitterAmount), spikeTransform.position.y);

            yield return new WaitForFixedUpdate();
        }
    }
}