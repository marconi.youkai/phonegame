﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using UnityEngine;
using UnityEngine.UI;

public class ThumbStickController : MonoBehaviour
{
    private Camera _camera;
    [SerializeField] private Transform _controllerBase, _controllerThumbStick;
    private SpriteRenderer _baseRenderer, _thumbStickRenderer;
    private Vector2 _pointA, _pointB, _direction;
    public Vector2 Direction => _direction;
    private bool _touching;

    private void Start()
    {
        _camera = Camera.main;
        _baseRenderer = _controllerBase.GetComponentInChildren<SpriteRenderer>();
        _thumbStickRenderer = _controllerThumbStick.GetComponentInChildren<SpriteRenderer>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _pointA = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _camera.transform.position.z));
            _controllerThumbStick.position = _pointA;
            _controllerBase.position = _pointA;
            _baseRenderer.enabled = true;
            _thumbStickRenderer.enabled = true;
        }

        if (Input.GetMouseButton(0))
        {
            _touching = true;
            _pointB = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _camera.transform.position.z));
        }
        else
        {
            _touching = false;
        }
    }

    private void FixedUpdate()
    {
        if (_touching)
        {
            Vector2 thumbStickOffset = _pointB - _pointA;
            _direction = Vector2.ClampMagnitude(thumbStickOffset, 1.0f);
            _controllerThumbStick.position = new Vector2(_pointA.x + _direction.x, _pointA.y + _direction.y);
        }
        else
        {
            _direction = Vector2.zero;
            _baseRenderer.enabled = false;
            _thumbStickRenderer.enabled = false;
        }
    }
}