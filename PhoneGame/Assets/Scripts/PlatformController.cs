﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlatformController : MonoBehaviour
{
    [SerializeField] private GameObject platformPrefab;
    private GameObject[] _platformArray;
    private Queue<Transform> _PlatformTransQueue;


    [SerializeField] private float platformSpeed, platformAcceleration;

    private void Start()
    {
        _platformArray = new GameObject[10];
        _PlatformTransQueue = new Queue<Transform>(10);
        
        for (int i = 0; i < _platformArray.Length - 1; i++)
        {
            var newPlatf = Instantiate(platformPrefab);
            _platformArray[i] = newPlatf;
            // _PlatformTransQueue.Enqueue(newPlatf.transform);
            newPlatf.transform.position = new Vector2(newPlatf.transform.position.x, -5.0f);
            newPlatf.gameObject.SetActive(false);
        }

        StartCoroutine("Spawner", _platformArray);
        StartCoroutine("PlatformMover");
    }

    private void PlatformLooper()
    {
        foreach (var VARIABLE in _PlatformTransQueue)
        {
        }
    }

    private IEnumerator Spawner(GameObject[] plataforms)
    {
        int i = 0;
        float timer = 0;
        while (i < _platformArray.Length)
        {
            if (timer > 8.0f)
            {
                plataforms[i].SetActive(true);
                plataforms[i].transform.position = new Vector2(Random.Range(-3 + 1.5f, 3 - 1.5f), plataforms[i].transform.position.y);
                _PlatformTransQueue.Enqueue(plataforms[i].transform);
                timer = 0.0f;
                i++;
            }

            timer += Time.fixedDeltaTime;
            yield return null;
        }
    }

    private IEnumerator PlatformMover()
    {
        while (true)
        {
            if (_PlatformTransQueue != null)
            {
                foreach (Transform platform in _PlatformTransQueue)
                {
                    Vector2 position = platform.position;
                    platform.position = position + (Vector2.up * (platformSpeed * Time.fixedDeltaTime));
                }
            }

            yield return new WaitForFixedUpdate();
        }
    }
}