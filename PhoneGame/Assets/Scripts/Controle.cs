﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controle : MonoBehaviour
{
    public Transform player;
    public float speed = 5.0f;
    private bool touch = false;
    private Vector2 pointA;
    private Vector2 pointB;
    public Transform controle;
    public Transform baseDoControle;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            pointA = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));
            controle.transform.position = pointA * -1;
            baseDoControle.transform.position = pointA;
            controle.GetComponent<SpriteRenderer>().enabled = true;
            baseDoControle.GetComponent<SpriteRenderer>().enabled = true;
        }

        if (Input.GetMouseButton(0))
        {
            touch = true;
            pointB = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));
        }
        else
        {
            touch = false;
        }
    }

    private void FixedUpdate()
    {
        if (touch)
        {
            Vector2 offset = pointB - pointA;
            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
            movePlayer(direction);
            controle.transform.position = new Vector2(
                pointA.x + direction.x,
                pointA.y + direction.y
            );
        }
        else
        {
            controle.GetComponent<SpriteRenderer>().enabled = false;
            baseDoControle.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    void movePlayer(Vector2 direction)
    {
        player.Translate(direction * speed * Time.deltaTime);
    }
}