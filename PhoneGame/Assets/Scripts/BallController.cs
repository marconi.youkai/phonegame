﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private Transform _ballTrans;
    private Rigidbody2D _ballRb;
    private Vector3 _ballPos;
    [SerializeField, Range(0.0f, 10.0f)] private float _ballSpeed = 1.0f;
    private ThumbStickController _stickController;

    private void Start()
    {
        this._stickController = GameObject.FindObjectOfType<ThumbStickController>();
        this._ballTrans = this.transform;
        this._ballRb = this.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        MoveBall(_stickController.Direction);
    }

    public void MoveBall(Vector2 direction)
    {
        direction.y = 0.0f;
        this._ballRb.AddForce(direction * _ballSpeed, ForceMode2D.Impulse);
    }
}