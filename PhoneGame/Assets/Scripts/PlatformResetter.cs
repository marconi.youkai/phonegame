﻿using UnityEngine;

public class PlatformResetter : MonoBehaviour
{
    private Transform _platformTrans;

    private void Start()
    {
        this._platformTrans = this.transform;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("bounds"))
        {
            _platformTrans.position = new Vector2(Random.Range(-3 + 1.5f, 3 - 1.5f), _platformTrans.position.y * -1.0f);
        }
    }
}